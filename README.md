# Start Gate Bot
Not yet finished!

# Example usage telegram ReplyKeyboardMarkup api, using `/sendMessage` method

```
{
  "chat_id": XXXXX,
  "text": "Choose one of the selection below",
  "reply_markup": {
    "keyboard": [
      [
        "Yes",
        "No"
      ],
      [
        "this is a very long sentence this is a very long sentence this is a very long sentence"
      ],
      [
        "this is also very long sentence this is also very long sentence"
      ]
    ],
    "one_time_keyboard": true
  }
}

```

# Example dict update from Telegram

```
{'message': {'chat': {'first_name': 'xxx',
                'id': xxx,
                'last_name': 'xxx',
                'type': 'private',
                'username': 'xxx'},
        'date': xxx,
        'from': {'first_name': 'xxx',
                'id': xxx,
                'is_bot': False,
                'language_code': 'en-US',
                'last_name': 'xxx',
                'username': 'xxx'},
        'message_id': 54,
        'text': 'xxx'},
'update_id': xxx}
```