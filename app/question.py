from .message import Message
from .helper import get_url

class Question(Message):
    """ manages question and answer to the telegram user """
    def __init__(self, update, questions):
        super().__init__(update)
        if re.match(r'^[QA]\d\:\s*', self.text): 
            self.is_question = True if self.text[:1] is "Q" else False
            self.is_answer = True if self.text[:1] is "A" else False
            self.question_num = self.text[1:2]
            self.answer = self.text[4:]
            self.questions = questions
        else:
            ValueError(update)

    def check_answer(self):
        if self.is_answer:
            question = questions[self.question_num]
            answer = ""
            for choice in question["choices"]:
                if re.match(r"^\*{1}[a-zA-Z]+", choice):
                    answer = "A{}: {}".format(self.question_num,choice)
            return True if answer is self.answer else False
        else:
            return "question is passed, cannot be checked"
    
    def count(self):
        return self.questions.__len__()

    def current_question(self):
        if self.is_question:
            question = questions[self.question_num]
            return "Q{}: {}".format(next_question_num,question["question"])
        else:
            return None

    def current_choices(self):
        """Prepare ReplyKeyboardMarkup with choices"""
        if self.is_question:
            question = questions[self.question_num]
            # TODO: this is not the correct reply_markup - refer to README.md
            reply_markup = ["A{}: {}".format(self.question_num,choice) for choice in question["choices"]]
            return reply_markup
        else:
            # All questions answered
            return None

    def has_next_question(self):
        next_question_num = self.question_num + 1 if self.count > self.question_num else -1
        if next_question_num is not -1:
            return True
        else:
            # All questions answered
            return False

    def reply_message(self,reply_text="I can hear you!",reply_markup=None):
        """
            Reply to the sender

                :param reply_text: text to reply
                :param reply_markup: refer ReplyKeyboardMarkup type 
            https://core.telegram.org/bots/api/#replykeyboardmarkup
                :return: Response object from requests module
            http://docs.python-requests.org/en/master/api/#requests.Response
        """
        data = {}
        data["chat_id"] = self.chat_id
        data["text"] = reply_text
        if reply_markup:
            data["reply_markup"] = reply_markup
        r = requests.post(get_url("sendMessage"), json=data)
        return r

    def reply_next_question():
        self.reply_message()

    def __repr__(self):
        return 'Question/Answer: {} - {}'.format(self.text,self.name_or_username()) 

