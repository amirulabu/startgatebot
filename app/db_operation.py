# make the call from db accessible as an object 

"""
Structure of a row in dynamodb

{
  "first_name": "xxx",
  "last_name": "xxx",
  "answers": {
      "group_name_1": {
          1: true,
          2: true,
          3: true,
          4: true
      },
      "group_name_2": {
          1: true,
          2: true,
          3: true,
          4: true
      },
  },
  "state":{
      "group_name_1": "question_1",
      "group_name_2": "completed"
  },
  "user_id": 123123,
  "username": "xxx"
}

Possible state

- None(python) / null (json)
- "group_name_1-question_1" (this means he answered question 1, so show him question 2)
- "group_name_2-incomplete" (this means he answered all question, and not all is correct)
- "group_name_2-completed" (this means he answered all question, and all is correct)
- "group_name_2-link_triggered_once"

---

Another table for managing changing links every hour
{
    "group_name": "telegramgroupname",
    "link": "http://telegram-link-to-group"
}

"""

import boto3
import os

# dynamodb = boto3.resource(
#     'dynamodb',
#     aws_access_key_id = os.environ['BOTO3_AWS_ACCESS_KEY_ID'],
#     aws_secret_access_key = os.environ['BOTO3_AWS_SECRET_ACCESS_KEY'],
# )

dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8010")

table = dynamodb.Table(name='startgatebot')

def add_or_identify_user(user, table=table):
    # takes the user object and if the user is in the db, returns the information about user from db
    # if the user is not in the db, push user info to db and returns the user from db
    try:
        user_db = table.get_item(Key={'UserID':user['id']})["Item"]
        return user_db
    except KeyError:
        if 'username' not in list(user):
            user['username'] = None
        table.put_item(Item={
            'UserID':user['id'], 
            'first_name': user['first_name'], 
            'username': user['username'],
            'question': {'q1':'n','q2':'n','q3':'n','q4':'n'}
        })
        return table.get_item(Key={'UserID':user['id']})["Item"]

def check_user_questions(user, table=table):
    # check how much correct questions answered
    try:
        user_db = table.get_item(Key={'UserID':user['id']})["Item"]
        counter = 0
        for v in user_db['question'].values():
            if v == "c":
                counter = counter + 1
        return counter
        
    except KeyError:
        return "User is not in db"
    
def add_result(user, question, result, table=table):
    try:
        table.update_item(
            Key={'UserID':user['id']},
            UpdateExpression='SET question.#question = :result', 
            ExpressionAttributeNames={'#question': question}, 
            ExpressionAttributeValues={':result': result}
        )
    except KeyError:
        return "User is not in db"