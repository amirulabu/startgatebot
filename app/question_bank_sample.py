questions={
    "telegramgroup1": {
        1: {
            "question": "This is question no 1",
            "choices": [
                "wrong answer",
                "wrong answer",
                "wrong answer",
                "*correct answer",
            ]
        },
        2: {
            "question": "This is question no 2",
            "choices": [
                "*correct answer",
                "wrong answer",
                "wrong answer",
                "wrong answer",
            ]
        },
        3: {
            "question": "This is question no 3",
            "choices": [
                "wrong answer",
                "wrong answer",
                "*correct answer",
                "wrong answer",
            ]
        },
        4: {
            "question": "This is question no 4",
            "choices": [
                "wrong answer",
                "wrong answer",
                "wrong answer",
                "*correct answer",
            ]
        },
    }
}