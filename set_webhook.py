from pprint import pprint
import requests
import sys
import os
TOKEN = os.environ['TELEGRAM_TOKEN_SGBOT']

"""
USAGE: python set_webhook.py <link>

link --  the ngrok link used to
tunnel the flask app.py development server 
"""

test_url = "{}/{}".format(str(sys.argv[1]),TOKEN)

def get_url(method):
    return "https://api.telegram.org/bot{}/{}".format(TOKEN,method)

r = requests.get(get_url("setWebhook"), data={"url": test_url})
r = requests.get(get_url("getWebhookInfo"))
pprint(r.status_code)
pprint(r.json())