import requests
from flask import Flask, request
from app.helper import get_url
from app.message import Message
from app.question import Question
from question_bank import questions
import datetime

import os
TOKEN = os.environ['TELEGRAM_TOKEN_SGBOT']


app = Flask(__name__)

def process_message(update):
    text = update["message"]["text"]
    # state = # to check state of the user 
    if(text.lower() == "/start"):
        message = Message(update)
        message.reply_message(reply_text="you send /start!")
    elif(text.lower() == "/about"):
        message = Message(update)
        message.reply_message(reply_text="you send /about!")
    elif(text.lower() == "/help"):
        message = Message(update)
        message.reply_message(reply_text="you send /help!")
    elif(text.lower() == "/link"):
        # get_state = get state-group name from db, 
        # check if = the state-group is completed, if question or link_triggered_once ask to answer again
        message = Message(update)
        message.reply_message(reply_text="you send /link! your link is http://something and will expire in TIME-minutes")
    elif(text.lower() == [k.lower() for k in questions.keys()]):
        # save "answer": {text.lower(): {1: False,2: False,3: False,4: False}
        # change "state": "text.lower()-incomplete"
    elif(re.match(r'^[A]\d\:\s*', text)):
        question = Question(update)
        # get_prev_state = from db question.chat_id
        # groupname = from get_prev_state using regex
        if question.check_answer():
            # save True at row["answers"][group_name_1][question.question_num]
            # change "state": "groupname-question_{}".format(question.question_num)
        else: 
            # save False at row["answers"][group_name_1][question.question_num]
            # change "state": "groupname-question_{}".format(question.question_num)
    else:
        message = Message(update)
        message.reply_message(reply_text="text")

@app.route("/{}".format(TOKEN), methods=["POST"])
def process_update():
    if request.method == "POST":
        update = request.get_json()
        if "message" in update:
            print("Update: ", update)
            process_message(update)
        return "ok", 200

@app.route("/{}/changelink".format(TOKEN), methods=["GET","POST"])
def process_update():
    # this link need to set to trigger every hour to make the link change every hour
    # for groupname in questions.key()
    #    data = {}
    #    data["chat_id"] = groupname
    #    r = requests.post(get_url("exportChatInviteLink"), json=data)
    return "ok", 200